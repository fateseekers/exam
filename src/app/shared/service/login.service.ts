import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './masks/user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private router: Router) { }

  selectedUser;

  logOut() {
    this.selectedUser = User;
    this.router.navigate(['']);
  }
}
