import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BaseApi } from '../core/base-api';
import { User } from './masks/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends BaseApi {

  options: HttpHeaders;

  constructor(public http: HttpClient) {
    super(http);

    this.options = new HttpHeaders();
    this.options = this.options.set('Content-type', 'application/json');
  }

  async getUsers() {
    return this.get('users', this.options).toPromise();
  }

  async getCurrentUser(id) {
    return this.get('users/' + id, this.options).toPromise();
  }

  async postUsers(data: User) {
    return this.post('users', data, this.options).toPromise();
  }
  async putUser(id, data: User) {
    return this.put('users/' + id, data, this.options).toPromise();
  }
  async deleteUser(id) {
    return this.delete('users/' + id, this.options).toPromise();
  }
}
