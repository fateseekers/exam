export class Post {
    constructor(
        public userId: number,
        public id?: number,
        public theme?: string,
        public content?: string
    ){}
}
