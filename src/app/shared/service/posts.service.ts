import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BaseApi } from '../core/base-api';
import { Post } from './masks/post';
import { User } from './masks/user';

@Injectable({
  providedIn: 'root'
})
export class PostsService extends BaseApi {

  options: HttpHeaders;

  constructor(public http: HttpClient) {
    super(http);

    this.options = new HttpHeaders();
    this.options = this.options.set('Content-type', 'application/json');
  }

  async getAllPosts() {
    return this.get('posts', this.options).toPromise();
  }
  async getUserPosts(id) {
    return this.get('posts?userId=' + id, this.options).toPromise();
  }
  async getCurrentPosts(id) {
    return this.get('posts/' + id, this.options).toPromise();
  }
  async postPost(data: Post) {
    return this.post('posts', data, this.options).toPromise();
  }
  async putPost(id, data: Post) {
    return this.put('posts/' + id, data, this.options).toPromise();
  }
  async deletePost(id) {
    return this.delete('posts/' + id, this.options).toPromise();
  }
  async deleteUserPosts(id) {
    return this.delete('posts?userId=' + id, this.options).toPromise();
  }
}
