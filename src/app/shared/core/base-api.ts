import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BaseApi {

  private link = "http://localhost:3000/";

  constructor(public http: HttpClient) { }

  public getUrl(url: string = ''):string {
    return this.link + url;
  }

  public get(url: string = '', header: HttpHeaders):Observable<any> {
      let requestOptions = {
          headers: header
      }
    return this.http.get(this.getUrl(url), requestOptions);   
  }

  public post(url: string = '', data, header: HttpHeaders):Observable<any> {
    return this.http.post(this.getUrl(url), data, {headers: header});   
  }

  public put(url: string = '', data, header: HttpHeaders):Observable<any> {
    return this.http.put(this.getUrl(url), data, {headers: header});   
  }

  public delete(url: string = '', header: HttpHeaders):Observable<any> {
    return this.http.delete(this.getUrl(url), {headers: header});   
  }
}