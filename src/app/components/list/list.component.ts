import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { LoginService } from 'src/app/shared/service/login.service';
import { PostsService } from 'src/app/shared/service/posts.service';
import { Post } from 'src/app/shared/service/masks/post';
import { Router } from '@angular/router';
import { User } from 'src/app/shared/service/masks/user';
import { UsersService } from 'src/app/shared/service/users.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  addNote: FormGroup;
  addUser: FormGroup;

  constructor(
    private postsService: PostsService,
    private usersService: UsersService,
    private loginService: LoginService,
    private router: Router
  ) {


    this.addNote = new FormGroup({
      theme: new FormControl("", [
        Validators.required, 
        Validators.pattern(".{1,20}") ]),
      content: new FormControl("", [
        Validators.required, 
        Validators.pattern(".{1,20}") ])
    });

    this.addUser = new FormGroup({
      name: new FormControl("", [
        Validators.required, 
        Validators.pattern(".{1,20}") ]),
      surname: new FormControl("", [
        Validators.required, 
        Validators.pattern(".{1,20}") ])
    });

  }

  result = '';
  allPosts: Post[];
  posts: Post[];
  users: User[];
  selectedUser: User;

  postId;
  theme;
  content;

  letAddUsers: number = 0;
  letCheckUsers: number = 0;

  toggleUsers() {
    switch (this.letCheckUsers) {
      case 0:
        this.letCheckUsers = 1;
        break;
      case 1:
        this.letCheckUsers = 0;
        break;
    }
  }

  toggleAddUser() {
    switch (this.letAddUsers) {
      case 0:
        this.letAddUsers = 1;
        break;
      case 1:
        this.letAddUsers = 0;
        break;
    }
  }

  async update() {
    this.posts = await this.postsService.getUserPosts(this.selectedUser.id);
    this.allPosts = await this.postsService.getAllPosts();
    this.users = await this.usersService.getUsers();
  }

  async getId(type) {
    var emptyId = 0;
    for (var i = 0; i < type.length; i++) {
      if (emptyId < type[i].id) {
        emptyId = type[i].id;
      }
    }
    this.update();
    return Number(emptyId) + Number(1);
  }

  async ngOnInit() {
    this.selectedUser = this.loginService.selectedUser;
    if (this.selectedUser == undefined) {
      this.selectedUser = {
        name: '',
        surname: '',
        role: 'common'
      }
      this.router.navigate(['']);
    } else {
      this.update();
    }
  }

  async onAddNote() {
    var newNote = {
      id: await this.getId(this.allPosts),
      userId: this.selectedUser.id,
      theme: this.addNote.value.theme,
      content: this.addNote.value.content,
    }
    this.postsService.postPost(newNote);
    this.addNote.reset();
    this.result = "Запись добавлена";
    this.update();
  }

  async onAddUser() {
    var newUser = {
      id: await this.getId(this.users),
      name: this.addUser.value.name,
      role: 'common',
      surname: this.addUser.value.surname
    }
    this.usersService.postUsers(newUser);
    this.result = "Пользователь добавлен";
    this.addUser.reset();
    this.update();
  }

  async deleteUser(id){
    this.usersService.deleteUser(id);
    var usersPosts = await this.postsService.getUserPosts(id);
    console.log(usersPosts);
    if(usersPosts.length != 0){
      this.postsService.deleteUserPosts(id);
    }
    this.result = "Пользователь и его записи удалены";
    this.update();
  }

  async editPost(id) {
    var editedPost = await this.postsService.getCurrentPosts(id);
    console.log(editedPost);
    this.postId = editedPost.id;
    this.theme = editedPost.theme;
    this.content = editedPost.content;

  }

  async onUpdateNote() {
    var updatedPost = await this.postsService.getCurrentPosts(this.postId);
    var newUpdatedNote = {
      id: updatedPost.id,
      userId: this.selectedUser.id,
      theme: this.addNote.value.theme,
      content: this.addNote.value.content,
    }
    this.postsService.putPost(newUpdatedNote.id, newUpdatedNote);
    this.addNote.reset();
    this.result = "Запись изменена";
    this.update();
  }

  async deletePost(id) {
    this.postsService.deletePost(id);
    this.result = "Запись удалена";
    this.update();
  }

  async changeRole(id){
    var currentUser = await this.usersService.getCurrentUser(id);
    switch(currentUser.role){
      case 'admin':
        var updatedUser = {
          id: currentUser.id,
          name: currentUser.name,
          surname: currentUser.surname,
          role: 'common'
        }
        break;
      case 'common':
        var updatedUser = {
          id: currentUser.id,
          name: currentUser.name,
          surname: currentUser.surname,
          role: 'admin'
        }
        break;
    }
    this.usersService.putUser(updatedUser.id, updatedUser);
    this.result = "Роль изменена";
    this.update();
  }

  logOut() {
    this.result = "Выход выполнен";
    this.loginService.logOut();
  }

}
