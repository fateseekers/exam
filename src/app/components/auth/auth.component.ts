import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/shared/service/users.service';
import { User } from 'src/app/shared/service/masks/user';
import { LoginService } from 'src/app/shared/service/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  constructor(private usersService: UsersService,
              private loginService: LoginService,
              private router: Router){}

  users: User [];
  selectedUser: User;

  async ngOnInit(){
    this.users = await this.usersService.getUsers();
  }

 

  findUser(userId){
    for(let i=0; i<this.users.length; i++){
      if(this.users[i].id == userId){
        return this.users[i];
      }
    }
  }

  check(e){
    this.loginService.selectedUser = this.findUser(e);
    this.router.navigate(['list']);    
  }
}
