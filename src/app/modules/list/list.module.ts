import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from 'src/app/components/list/list.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { PostsService } from 'src/app/shared/service/posts.service';
import { UsersService } from 'src/app/shared/service/users.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  declarations: [
    ListComponent
  ],
  bootstrap: [
    ListComponent
  ],
  providers: [
    UsersService,
    PostsService
  ]
})
export class ListModule { }
