import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersService } from 'src/app/shared/service/users.service';
import { AuthComponent } from 'src/app/components/auth/auth.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { PostsService } from 'src/app/shared/service/posts.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule
  ],
  declarations: [
    AuthComponent
  ],
  bootstrap: [ 
    AuthComponent
  ],
  providers: [
    UsersService,
    PostsService
  ]
})
export class AuthModule {}